json.array!(@rangers) do |ranger|
  json.extract! ranger, :id, :name, :color
  json.url ranger_url(ranger, format: :json)
end
