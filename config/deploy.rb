#$:.unshift(File.expand_path('./lib', ENV['rvm_path'])) # Add RVM's lib directory to the load path.
require 'rvm/capistrano'                               # Load RVM's capistrano plugin.
require 'bundler/capistrano'

set :current_path, "/webapps/freeranger/current"

set :stages, %w(vagrant)
set :default_stage, 'vagrant'

set :application,     'freeranger'

require 'capistrano/ext/multistage'

set :domain,          'freeranger'
set :deploy_to,       '/webapps/freeranger'

set :rvm_ruby_string, "2.0.0-p195@global"             # Or whatever env you want it to run in.
set :rvm_type,        :system                         # Copy the exact line. I really mean :user here

set :repository,      'https://quangquach@bitbucket.org/quangquach/rails-deploy-demo-app.git'

set :branch,          'master'

set :scm,             :git
set :scm_verbose,     true

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
ssh_options[:auth_methods] = ["publickey"]

set :keep_releases,   50

set :bundle_dir,  ""
set :bundle_flags, ""
set :bundle_without, []

set :unicorn_pid, "#{shared_path}/pids/unicorn.pid"
set(:current_path)      { File.join(deploy_to, current_dir) }

namespace :deploy do
  task :warning do
    puts "*********** DEPLOYING TO #{stage.upcase} **********"
    print "ARE YOU SURE YOU WANT TO DO THIS? (y or n): "
    input = STDIN.gets.strip.downcase
    if input != 'y'
      abort("ABORTING DEPLOYMENT")
    end
  end

  desc "Start server"
  task :start do
    run "cd #{current_path} ; bundle exec unicorn_rails -c config/unicorn.rb -D -E production"
  end

  desc "Stop Server"
  task :stop do
    run "kill -s QUIT `cat #{unicorn_pid}`"
  end

  desc "Restart Server"
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "rvmsudo kill -s USR2 `cat #{unicorn_pid}`"
  end

  task :symlink_data_to_shared_data do
    run "ln -s #{File.expand_path(File.join(current_path, '/../shared/data'))} #{current_path}/data"
  end

  desc "Create socket file symlink for nginx"
  task :symlink_sockets, :except => {:no_release => true} do
    run "mkdir -p #{shared_path}/sockets"
    run "rm -rf #{release_path}/tmp/sockets"
    run "ln -s #{shared_path}/sockets #{release_path}/tmp"
  end

  desc "Create vendor/cache file symlink"
  task :symlink_vendor_cache, :except => {:no_release => true} do
    run "mkdir -p #{shared_path}/vendor_cache"
    run "rm -rf #{release_path}/vendor/cache"
    run "ln -s #{shared_path}/vendor_cache #{release_path}/vendor/cache"
  end

  desc "Create file symlink"
  task :symlink_files, :except => {:no_release => true} do
    run "mkdir -p #{shared_path}/files"
    run "rm -rf #{release_path}/tmp/files"
    run "ln -s #{shared_path}/files #{release_path}/tmp/files"
  end

  shared_children.push "tmp/sockets"

  task :fix_permissions do
    run "#{try_sudo} chown #{user}:#{user} -R #{shared_path}/.." # nginx run as www-data user
  end

  task :pack_gems, :roles => [:app, :worker], :except => {:no_release => true} do
    run "cd #{current_path} && bundle pack"
  end

  namespace :assets do
    desc "Precompile assets locally and then rsync to app servers"
    task :precompile do
      #run_locally "mkdir -p public/__assets; mv public/__assets public/assets;"
      servers = find_servers :roles => [:app], :except => { :no_release => true }
      servers.each do |server|
        #run_locally "rsync --rsh='ssh -i #{ssh_key}' -av ./public/assets/ #{user}@#{server}:#{current_path}/public/assets/;"
        run "source /usr/local/rvm/environments/default;cd #{deploy_to}/current ;RAILS_ENV=production bundle exec rake assets:precompile;"
      end
      #run_locally "mv public/assets public/__assets"
    end
  end
end

namespace :config do

  desc "copy all necessary config"
  task :batch do
    config.database
    config.unicorn
  end

  task :database do
    copy_config('database.yml')
  end

  task :unicorn do
    copy_config('unicorn.rb')
  end

  def copy_config(file_name)
    run "rm -f #{current_path}/config/#{file_name}"
    run "ln -s #{shared_path}/config/#{file_name} #{current_path}/config/#{file_name}"
  end
end

namespace :rake do
  desc "Run a task on a remote server."
  # run like: cap staging rake:invoke task=a_certain_task
  task :invoke, :roles => :app do
    hostname = find_servers_for_task(current_task).first
    command = <<-EOS
    ssh -i #{ssh_key} #{user}@#{hostname} 'source /usr/local/rvm/environments/default;cd #{deploy_to}/current ; RAILS_ENV=#{rails_env} bundle exec rake #{ENV['task']}'
    EOS
    #run("cd #{deploy_to}/current;bundle exec rake #{ENV['task']} RAILS_ENV=#{rails_env}")
    puts command
    exec command
  end
end

namespace :puppet do
  task :apply_web, :roles => :app do
    servers = find_servers_for_task(current_task)
    servers.each do |hostname|
      command = <<-EOS
    ssh -i #{ssh_key} #{user}@#{hostname} 'source /usr/local/rvm/environments/default; cd /etc/puppet/; git pull; rvmsudo puppet apply /etc/puppet/manifests/app.pp --debug --verbose --environment #{stage}'
      EOS
      run_locally command
    end
  end
end

before 'deploy', 'deploy:warning', 'deploy:fix_permissions'
after 'deploy',
      'config:batch',
      'deploy:symlink_data_to_shared_data',
      'deploy:symlink_sockets',
      'deploy:assets:precompile',
      'deploy:cleanup',
      'deploy:symlink_vendor_cache', # reserve folder before packing gems
#      'deploy:pack_gems',#'sidekiq:restart',
      'rails:restart' # zero downtime
